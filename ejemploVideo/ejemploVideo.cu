#include <cuda.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <stdio.h>

using namespace std;
using namespace cv;


void convertFrameToGrayscale(uchar *datosframeEntrada,uchar *datosFrameSalida,int width,int height);

int main (int argc, char **argv)
{
	if (argc!=3)
	{
		printf("[ERR] Usage: %s Input.avi Output.avi\n",argv[0]);
		exit (0);

	}
	VideoCapture videoEntrada(argv[1]);

	if(!videoEntrada.isOpened())
	{
		printf("[ERR] Couldn't Open file : %s as an input video \n",argv[0]);
		exit(0);
	}

	int width = videoEntrada.get(CV_CAP_PROP_FRAME_WIDTH);
	int height= videoEntrada.get(CV_CAP_PROP_FRAME_HEIGHT);



	Size videoSize(width,height);

	VideoWriter videoSalida(string(argv[2]),
			CV_FOURCC('M','J','P','G'),
			videoEntrada.get(CV_CAP_PROP_FPS),
			videoSize);

	if(!videoSalida.isOpened())
		{
			printf("[ERR] Couldn't Open file : %s as an output video \n",argv[2]);
			videoEntrada.release();
			exit(0);
		}

	Mat frameCapturado,frameSalida;
	uchar *datosFrameSalida=(uchar *)malloc(
			width*height*3*sizeof(uchar));

	while(videoEntrada.read(frameCapturado))
	{
		convertFrameToGrayscale(frameCapturado.data,
				datosFrameSalida,
				width,
				height);
		frameSalida = Mat (videoSize, CV_8UC3, datosFrameSalida);
		videoSalida.write(frameSalida);

	}

	free(datosFrameSalida);
	videoEntrada.release();
	videoSalida.release();
	return 0;

}

void convertFrameToGrayscale(uchar *datosframeEntrada,uchar *datosFrameSalida,int width,int height)
{
	for (int i=0; i <height; i++)
		for(int j =0 ; j < width; j++)
		{
			size_t pixelIdx =3*(i*width+j);
			uchar b= datosframeEntrada [pixelIdx];
			uchar g= datosframeEntrada [pixelIdx +1];
			uchar r= datosframeEntrada [pixelIdx + 2];
			uchar val = round((float)r*0.21 + (float)g*0.72 + (float)b*0.07);

			datosFrameSalida[pixelIdx]=val;
			datosFrameSalida[pixelIdx+1 ]=val;
		    datosFrameSalida[pixelIdx + 2]=val;

		}
}
